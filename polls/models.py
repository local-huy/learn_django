from django.db import models
from django.db.models.fields import CharField
from django.utils import timezone
import datetime

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    email_account = models.EmailField(default="huyngo@natrol.vn")
    def __str__(self):
        return self.question_text
    def was_published_recently(self):
        now = timezone.now()
        #return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    
    def get_field_question(self):
        return [(field.name,field.value_to_string(self)) for field in Question._meta.fields]
        #return [Question._meta.get_fields()]

class Question_New(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    email_account = models.EmailField(default="huyngo@natrol.vn")

class Choice(models.Model):
    question = models.ForeignKey(Question,on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
# Create your models here.
    def __str__(self):
        return self.choice_text   


class Person(models.Model):    
    SHIRT_SIZES = (
      ('S','small'),
      ('M','medium'),
      ('L','large'),    
    )        
    shirt_size = models.CharField(max_length=1, choices=SHIRT_SIZES)
    name = models.CharField(max_length=200)
    
    def __str__(self):
        return self.name
    
class Group(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name
    
class Membership(models.Model):
    
    person = models.ForeignKey(Person,on_delete=models.CASCADE)
    group = models.ForeignKey(Group,on_delete=models.CASCADE)
    date_join = models.DateField()
    invite_reason = models.TextField()    
    def __str__(self):
        return "{} {}".format(self.person,self.group)


class Blog(models.Model):

    name = models.CharField(max_length=100) 
    tag_line = models.TextField()   

    def save(self,*args,**kwargs): 

        if self.name == "Huy":
            print("{} don't save his own".format(self.name))
        else:
            super(Blog,self).save(*args,**kwargs)

    def __str__(self):
        return self.name


class Document(models.Model):

    description = models.CharField(max_length=200)
    image = models.ImageField(upload_to='polls/static/')
    uploaded_at = models.DateTimeField(auto_now_add=True)