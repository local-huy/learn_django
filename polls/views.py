from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from .models import Question, Choice, Blog, Person, Membership
from django.urls import reverse
from django.views import generic
from .form import ContactForm,FileForm
from django.core.mail import send_mail
from django.conf import settings

class IndexView (generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_result'

    def get_queryset(self):
        
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]
    


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/result.html'
    
    
def index(request):
    latest_result = Question.objects.filter(question_text__startswith='what')    
    #latest_result = Question.objects.order_by('-pub_date')[:5]
    v_persons = Person.objects.all()
    v_memberships = Membership.objects.all()
    form = ContactForm()    
    #output = ', '.join(q.question_text for q in latest_result)
    context = {'latest_result': latest_result,'v_persons': v_persons,'v_memberships': v_memberships, 
              'form': form.as_ul()}    
    return render(request,'polls/index.html',context)


def file_form(request):
    
    if request.method == 'POST': 
        file_form = FileForm(request.POST,request.FILES)        
        if file_form.is_valid():
            file_form.save()
            return HttpResponseRedirect(reverse('polls:index'))
    else:
        file_form = FileForm()

    return render(request, 'polls/form.html', {'file_form': file_form.as_ul()})        
 



def get_contactform(request,person_id):    
    if request.method == 'POST':
        form = ContactForm(request.POST)
        


        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            email = form.cleaned_data['sender']
            sender = 'ndahuy@tma.com.vn'
            cc_myself = form.cleaned_data['cc_myself']

            q=Person.objects.get(id=person_id)
            q.name=subject
            q.save()
            #print(request.META)
            print("path = {}".format(request.path))
            print("path_info = {}".format(request.path_info))
            khach = ['huyngo@natrol.vn']
            print(settings.BASE_DIR)
            if cc_myself: 
                khach.append('ndahuy@tma.com.vn')

            #send_mail(subject,message,sender,khach)

            ### Print to console

            """
                Get contactform, handle form and save the status of subject to the name of person. 

            """
            for k, v in form.cleaned_data.items(): 
                print ("Key = {}, value = {}".format(k,v))
            return HttpResponseRedirect(reverse('polls:persons',args = (person_id,)))    
            #return render(request, 'polls/person.html', {'form': form.cleaned_data ,'person': person})        

        else:
            form = ContactForm()
            form.as_ul()

        if  file_form.is_valid():
            file_form.save()
            return HttpResponseRedirect(reverse('polls:persons',args = (person_id,)))  



            
    else:         
        form = ContactForm()          
        person=Person.objects.get(id=person_id)      
        return render(request, 'polls/person.html', {'form': form.as_ul(),'person': person})        
    #return render(request, reverse('polls:persons',args = (person_id,)), {'form': form})        


def persons(request,person_id):    
    person = get_object_or_404(Person,id=person_id)
    context = {'person': person}
    return render(request,'polls/person.html',context)    

def details(request,question_id):    
    #try: 
    #    question = Question.objects.get(id=question_id)
    #except Question.DoesNotExist: 
    #    raise Http404("Question does not exist")
    
    question = get_object_or_404(Question,id=question_id)    
    context = {'question': question}
    return render(request,'polls/detail.html',context)    
    #return HttpResponse("This is detail view {}".format(question_id))

def results(request,question_id):
    question = get_object_or_404(Question,id=question_id)
    return render(request,'polls/result.html',{'question': question})
    #return HttpResponse("This is result view {}".format(question_id))

def vote(request,question_id):
    question = get_object_or_404(Question,id=question_id)
    try: 
        select_choice = question.choice_set.get(id=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist): 
        return render(request,'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice",        
        })    
    
    else: 
        
        select_choice.votes+=1
        select_choice.save()
        return HttpResponseRedirect(reverse('polls:results',args = (question_id,)))    


        
    #return HttpResponse("This is vote for question {}".format(question_id))
# Create your views here.
