from django.conf.urls import url
from . import views
from django.conf.urls.i18n import urlpatterns

app_name = 'polls'

urlpatterns = [
    url(r'^$',views.index,name='index'),
    #url(r'^$',views.IndexView.as_view(),name='index'),
    
    url(r'^(?P<question_id>[0-9]+)/$',views.details,name="details"),

    #url(r'^(?P<question_id>[0-9]+)/$',views.DetailView.as_view(),name="details"),
    
    url(r'^(?P<person_id>[0-9]+)/person/$',views.persons,name="persons"),


    url(r'^(?P<question_id>[0-9]+)/results/$',views.results,name="results"),
    #url(r'^(?P<question_id>[0-9]+)/results/$',views.DetailView.as_view(),name="results"),
    
    url(r'^(?P<question_id>[0-9]+)/vote/$',views.vote,name="vote"),
    url(r'^file_form/$',views.file_form,name="file_form"),

    url(r'^(?P<person_id>[0-9]+)/form/$',views.get_contactform,name="get_contactform"),  
    
    
]