from django import forms
from django.conf import settings
from .models import Document

class ContactForm(forms.Form):
    subject = forms.CharField(label='Subject',max_length=200)
    message = forms.CharField(label='Message',max_length=400)
    sender = forms.EmailField(label='Email')
    cc_myself = forms.BooleanField(label='CC',required=False)


class FileForm(forms.ModelForm):    
	class Meta: 
		model = Document
		fields = ('description','image',)

    
    
    